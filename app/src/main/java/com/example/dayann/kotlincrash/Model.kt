package com.example.dayann.kotlincrash

import kotlin.math.roundToLong

data class Model(private val cal: Double) {
    fun text() = cal.roundToLong().toString()
}